﻿using Microsoft.AspNetCore.Mvc;
using PlayerAgentManagement.BL;



namespace PlayerAgentManagement.UI.Web.MVC.Controllers;

public class AgentController : Controller
{
    private readonly IManager _manager;
    
    public AgentController(IManager manager)
    {
        _manager = manager;
    }
    
    [HttpGet]
    public IActionResult Index()
    {
        var agents = _manager.GetAllAgentsWithPlayers();
        return View(agents);
    }
    
    [HttpGet]
    public IActionResult Details(int id)
    {
        var agent = _manager.GetAgentWithPlayers(id);
        return View(agent);
    }
}