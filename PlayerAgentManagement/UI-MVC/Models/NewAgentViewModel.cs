﻿using System.ComponentModel.DataAnnotations;

namespace PlayerAgentManagement.UI.Web.MVC.Models;

public class NewAgentViewModel
{
    [Required]
    [MaxLength(50)]
    public string Name { get; set; }
    [Required]
    [DataType(DataType.Date)]
    public DateTime BirthDate { get; set; }
    [MaxLength(20)] 
#nullable enable
    public string? PhoneNumber { get; set; }
    public int Id { get; set; }
}