﻿using System.ComponentModel.DataAnnotations;
using PlayerAgentManagement.BL.Domain;

namespace PlayerAgentManagement.UI.Web.MVC.Models;

public class NewTeamViewModel
{
    [MinLength(2)]
    public string Name { get; set; }
    [DataType(DataType.Date)]
    public DateTime Founded { get; set; }
    [EnumDataType(typeof(Position))]
    public Position Position { get; set; }
    [StringLength(30, MinimumLength = 3)]
    public string Country { get; set; }
    public int Id { get; set; }
    
    
}